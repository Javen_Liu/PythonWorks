import logging

log_filename = "c:/python27/lib/philo/log.txt"
logging.basicConfig(filename = log_filename, level = logging.INFO)
logging.debug("I think this shouldn't be seen in the log")
logging.info("\n\nThis will be appearred on the first line in the log!\n\n")
# logging has four level of error infomation. 
# they are: debug, info, warning, error, orderred from the most detailed to the least
 




def func(s):
	n = int(s)
	return 10 / n

def bar(s):
	try:
		return func(s)
	except StandardError, e:
		print 'Error, oh no!'
	finally:
		logging.exception(e)
		print "end"


		#the way using logging

bar(0)