from types import MethodType

class Sample(object):
	__slots__ = ("test", "demo", "other")
# __slots__ is used to limit the attribute of a class

def sinput(self,string):
	s.demo = "what you say : %s " % string
 	print s.demo

Sample.sinput = MethodType(sinput, None, Sample)
 #CLASS.method = MethodType("function name", "instance name", class name)

s = Sample()
s.test = "test"
s.sinput("hello")

d = Sample()
d.other = 2
print d.other


#  "s.method" can be transformed into a property by using @property
# use @property to make a method become a property which can be read
# and then use "@instancename.setter" to make it able to be written

 







