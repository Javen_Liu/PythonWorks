# sequence_sum is used to sum up a set of consecutive digit whose range is from stn to enn
def sequence_sum(stn,enn):
	a = range(stn,enn+1)
	return reduce(lambda x,y: x + y , a)

a = sequence_sum(1,4)
print a