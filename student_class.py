class Student(object):
	"""docstring for Student"""
	#in a class, all definition should have a "self".

	def __init__(self, name, num, score):
		self.name = name
		self.num  = num
		if 0 <= score <= 100:
			self.__score = score
		else:
			raise ValueError('inaviable number!')


	def show(self):
		print "name: %s, schoolnumber: %s, score: %s." %(
			self.name, self.num, self.__score)
	
	def get_score(self):
		return self.__score
		# user outside can get the private value score

	def set_score(self, a):
		if 0 <= a <= 100:
			self.__score = a
		else: 
			raise ValueError('inaviable number')

zhe = Student('zhe', 26, 60)
zhe.show()
zhe.name = "col"
zhe.num = 3
zhe.set_score(99)
zhe.show()
