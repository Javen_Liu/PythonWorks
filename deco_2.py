
import logging

def deco_h(*s):
	def deco(func):
		def wrapper(*args, **kw):	
			print "begin"
			func(*args, **kw)
			try:
				string = s
				print "asda %s %s" %string
			except StandardError, e:
				print "@deco_h() input error"
				logging.exception(e)
			else:
				pass
			print "end"	
		return wrapper
	return deco

	

@deco_h(1,1,1)
def f(text):
	print "function running, what you say: %s" % text
	return

f("hello")
